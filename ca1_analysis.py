"""

CA1 ECG analysis

Student name: Sean Duignan D00189797
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
# imports go here (anything in numpy/scipy is fine)

# re-usable functions / classes go here

time_samples = []
first_ecg = []
annotation_times = []   

###############################################################################

def analyse_ecg(signal_filename, annotations_filename):
    
    # basic formatted output in Python
    print('signal filename: %s' % signal_filename)
    print('annotation filename: %s' % annotations_filename)
    read_files(signal_filename, annotations_filename)
    show_plot()
    print_summary(first_ecg)
    
###############################################################################

def read_files(input_signal, annotation):
    
    global time_samples, first_ecg, second_ecg, annotation_times
    time_samples, first_ecg, second_ecg = np.loadtxt(input_signal, usecols = (0,1,2,), skiprows=2, unpack=True, delimiter=',')
    annotation_times = np.loadtxt(annotation, usecols = (0,), skiprows=1, unpack=True)

###############################################################################

def show_plot():
    
    plt.plot(time_samples, first_ecg,'r',label='ECG1')
    plt.plot(time_samples, first_ecg,'g',label='ECG2')
    
    beats = beat_peaks(first_ecg)
    for b in range(len(beats)):
        plt.annotate('r', xy = (beats[b], 1))
    
    for a in range(len(annotation_times)):
        plt.annotate('a', xy = (annotation_times[a], 2))

    
    plt.title('Electrocardiogram')
    plt.ylabel('Voltage (mV)')
    plt.xlabel('Seconds')

    plt.legend()
    plt.grid()
    plt.show()    

###############################################################################

def beat_peaks(ecg):
    
    peaks = []
    
    for i in range(1, len(ecg)-1):
        # sample > previous    and sample > next        and voltage > 1V
        if (ecg[i] > ecg[i-1]) and (ecg[i] > ecg[i+1]) and (ecg[i] > 1):
            
            peaks.append(time_samples[i])
            
    return peaks

###############################################################################       

def print_summary(ecg):
    
    print('Sampling rate: {0} Hz'.format(sampling_frequency(time_samples)))
    print('Data Length: {0} samples'.format(no_of_samples(time_samples)))
    print('Time Length: {0} s'.format(time_length(time_samples)))
    print('Min signal value: {0} mV'.format(min_signal_value(ecg)))
    print('Max signal value: {0} mV'.format(max_signal_value(ecg)))
    print(len(beat_peaks(first_ecg)), " heart beats detected!")

###############################################################################

def sampling_frequency(alist):
    
    return no_of_samples(alist) / time_length(alist)
    
###############################################################################

def no_of_samples(alist):
    return len(alist)
  
###############################################################################
  
def time_length(alist):
    
    return alist[len(alist)-1] - alist[0]
    

###############################################################################
  
def min_signal_value(ecg):
    return min(ecg)

###############################################################################
  
def max_signal_value(ecg):
    return max(ecg)

###############################################################################

    

if __name__=='__main__':
    
    analyse_ecg(sys.argv[1] + '_signals.txt', sys.argv[1]+'_annotations.txt')

