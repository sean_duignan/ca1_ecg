import unittest
from ca1_analysis import min_signal_value, max_signal_value


class CA1TestCase(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def test_min_signal_value(self):
        alist = [6,67,23,15,90,56,72]
        self.assertEqual(6, min_signal_value(alist))
        #self.assertNotEqual(56, min_signal_value(alist))
    
    def test_max_signal_value(self):
        alist = [6,67,23,15,90,56,72]
        self.assertEqual(90, max_signal_value(alist))
        #self.assertNotEqual(72, max_signal_value(alist))
 
    if __name__ == '__main__':
        unittest.main()