CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): Sean Duignan

Sortable name (no spaces, surname then firstname): sean_duignan

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

YOUR ANSWERE HERE.
If I had more time I would have improved on the display of the plot. I had a little bit of trouble finding easy to understand material in this area.
I understand sampling theory quite well, and found the data easy to work with.

Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)

YOUR ANSWERE HERE.
I got acquainted with python, learned a bit on version control technologies and testing exercise.

Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

YOUR ANSWER HERE.
Likes are answered in previous question. I found sourcing of material on how to use matplotlib's annotations and styling to be a bit tedious.

